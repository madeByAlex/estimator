import json
import click
from flask.cli import AppGroup

from application import app, db
from .forms import FillBuildingColumn, FillBuildingCoords
from .models import Building


building = AppGroup("building")


@building.command("fill-column-value")
@click.argument("column_name", type=click.Choice(["floors", "material", "type", "garbage_chute", "heating_type", "year"]))
def building_fill_column_value(column_name):
    fbc = FillBuildingColumn(column_name)
    fbc.load_buildings()\
       .fill_strong_items()\
       .fill_medium_items()\
       .update_column()

@building.command("fill-geography")
def building_fill_geography():
    for b in Building.query.filter_by(district=None).all():
        FillBuildingCoords(b)\
            .fill_coords()\
            .fill_district()\
            .update()




app.cli.add_command(building)