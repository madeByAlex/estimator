from sqlalchemy.dialects.postgresql import JSON
from sqlalchemy.orm import validates


from application import db
from . import labels as LABELS


class Building(db.Model):
    """
    """

    id = db.Column(db.Integer, primary_key=True)

    # Тип строения(новостройка, вторичка)
    type = db.Column(db.SmallInteger)

    lat = db.Column(db.Float)

    lng = db.Column(db.Float)

    # Город(Москва, Санкт-Петербург, ...)
    city = db.Column(db.SmallInteger, nullable=False)

    district = db.Column(db.SmallInteger)

    # Улица(Новый Арбат, ...)
    street = db.Column(db.String, nullable=False)

    # Номер и корпус дома(1А, ...)
    house = db.Column(db.String, nullable=False)

    # Год постройки дома(1900-202.)
    year = db.Column(db.SmallInteger)

    # Материал стен
    material = db.Column(db.SmallInteger)

    # Количество этакжей в доме
    floors = db.Column(db.SmallInteger, nullable=False)

    # Количество лифтов(количество[пассажирских, грузовых])
    elevators = db.Column(JSON)

    # Количество парковок(количество[подземных, наземных])
    parkings = db.Column(db.SmallInteger)

    # Наличие мусоропровода
    garbage_chute = db.Column(db.Boolean)

    # Тип отопления дома(центральное, индивидуальное, ...)
    heating_type = db.Column(db.SmallInteger)

    counters = db.Column(JSON)

    metrics = db.Column(JSON)

    flats = db.relationship("Flat", backref="building", lazy="dynamic")

    created_on = db.Column(db.DateTime, default=db.func.now())

    updated_on = db.Column(db.DateTime, default=db.func.now(), onupdate=db.func.now())


class Flat(db.Model):
    """
    """

    id = db.Column(db.Integer, primary_key=True)

    # Идентификатор внешний
    external_id = db.Column(db.Integer, nullable=False)

    # Источник парсинга
    source = db.Column(db.SmallInteger, nullable=False)

    # Идентификатор автора
    owner_id = db.Column(db.Integer)

    # Тип автора (агент, компания, ...)
    owner_type = db.Column(db.SmallInteger)

    # Цена заявленная на сайте
    price = db.Column(db.Float, nullable=False)

    # Общая площадь квартиры (м**2)
    area = db.Column(db.Float, nullable=False)

    # Жилая площаль (м**2)
    area_kitchen = db.Column(db.Float)

    # Кухни площадь (м**2)
    area_living = db.Column(db.Float)

    # Этаж квартиры
    floor = db.Column(db.SmallInteger, nullable=False)

    # Кличество комнат
    rooms = db.Column(db.SmallInteger, nullable=False)

    # Планировка комнат
    layout = db.Column(db.SmallInteger)

    # Высота потолков (м)
    height = db.Column(db.Float)

    # Количество сан.узлов(количество[совмещенных, раздельных])
    bathrooms = db.Column(JSON)

    # Вид из окон(бинарно[во двор, на улицу])
    window_types = db.Column(JSON)

    # Количество балконов и лоджий(количество[балконов, лоджий])
    balconies = db.Column(JSON)

    # Состояние ремонта(без ремонта, дизайнерский, косметический, ...)
    condition = db.Column(db.SmallInteger)

    # Контактные телефоны(список[])
    phones = db.Column(JSON)

    building_id = db.Column(db.Integer, db.ForeignKey("building.id"), nullable=False)

    created_on = db.Column(db.DateTime, default=db.func.now())

    updated_on = db.Column(db.DateTime, default=db.func.now(), onupdate=db.func.now())

