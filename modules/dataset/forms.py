import re
from wtforms import Form
from wtforms import validators
from wtforms import StringField, IntegerField, FloatField, FieldList, Field, BooleanField
from sqlalchemy.orm.attributes import flag_modified
import pandas as pd
import numpy as np
import requests

from . import labels as LABELS
from .models import Building, Flat
from application import db, app


def validate_integer(form, field):
    if not field.data is None and not isinstance(field.data, int):
        raise validators.ValidationError("Unexpected value: {}.".format(field.data))

def validate_float(form, field):
    if not field.data is None and not isinstance(field.data, float):
        raise validators.ValidationError("Unexpected value: {}.".format(field.data))


class CreateForm(Form):
    """
    """

    external_id = IntegerField(None, [
        validators.DataRequired(),
        validate_integer
    ])

    source = IntegerField(None, [
        validators.DataRequired()
    ])

    rooms = IntegerField(None, [
        validators.DataRequired(),
        validate_integer
    ])

    area = FloatField(None, [
        validators.DataRequired(),
        validate_float
    ])

    floor = IntegerField(None, [
        validators.DataRequired(),
        validate_integer
    ])

    price = FloatField(None, [
        validators.DataRequired(),
        validate_float
    ])

    floors = IntegerField(None, [
        validators.DataRequired(),
        validate_integer
    ])

    city = IntegerField(None, [
        validators.DataRequired()
    ])

    street = StringField(None, [
        validators.DataRequired()
    ])

    house = StringField(None, [
        validators.DataRequired()
    ])

    owner_id = IntegerField(None, [
        validate_integer
    ])

    def validate_source(self, field):
        if not field.data in LABELS.get("sources").keys():
            raise validators.ValidationError("Unexpected value: {}.".format(field.data))
        self.source.data = LABELS.get("sources").get(field.data)

    def validate_city(self, field):
        if not field.data in LABELS.get("cities").keys():
            raise validators.ValidationError("Unexpected value: {}.".format(field.data))
        self.city.data = LABELS.get("cities").get(field.data).get("id")

    def save(self):
        building_n, flat_n = False, False
        b = Building.query\
                    .filter_by(city=self.city.data,
                               street=self.street.data,
                               house=self.house.data)\
                    .first()
        if b is None:
            building_n = True
            b = Building(city=self.city.data,
                         street=self.street.data,
                         house=self.house.data,
                         floors=self.floors.data)
            db.session.add(b)
        if not Flat.query.filter_by(external_id=self.external_id.data).first():
            flat_n = True
            f = Flat(source=self.source.data,
                     external_id=self.external_id.data,
                     area=self.area.data,
                     price=self.price.data,
                     floor=self.floor.data,
                     rooms=self.rooms.data,
                     owner_id=self.owner_id.data)
            b.flats.append(f)
            db.session.add(b)
        db.session.commit()
        return building_n, flat_n


class UpdateForm(Form):
    """
    """

    phones = FieldList(
        StringField(None, [
            validators.DataRequired()
        ]), 1
    )

    material = IntegerField(None, [
        validate_integer
    ])

    floors = IntegerField(None, [
        validate_integer
    ])

    elevators = Field()

    parkings = Field()

    garbage_chute = IntegerField(None, [
        validate_integer
    ])

    heating_type = IntegerField(None, [
        validate_integer
    ])

    layout = IntegerField(None, [
        validate_integer
    ])

    owner_type = IntegerField(None, [
        validate_integer
    ])

    owner_id = IntegerField(None, [
        validate_integer
    ])

    area_living = FloatField(None, [
        validate_float
    ])

    area_kitchen = FloatField(None, [
        validate_float
    ])

    year = IntegerField(None, [
        validate_integer
    ])

    room_type = IntegerField(None, [
        validate_integer
    ])

    height = FloatField(None, [
        validate_float
    ])

    bathrooms = Field()

    window_types = Field()

    balconies = Field()

    condition = IntegerField(None, [
        validate_integer
    ])

    type = IntegerField(None, [
        validate_integer
    ])

    def to_int(self, attr):
        value = getattr(self, attr).data
        if isinstance(value, str):
            value = re.search(r"\d+", value)
            if value != "" and not value is None:
                getattr(self, attr).data = int(value.group())

    def to_float(self, attr):
        value = getattr(self, attr).data
        if isinstance(value, str):
            value = value.replace(",", ".")
            value = ".".join(re.findall(r"\d+", value))
            if value != "":
                try:
                    getattr(self, attr).data = float(value)
                except Exception as e:
                    app.logger.error(e)

    def to_index(self, attr):
        value = getattr(self, attr).data
        if value in LABELS.get(attr, {}):
            getattr(self, attr).data = LABELS.get(attr).index(value)

    def to_list(self, attr, matches):
        value = getattr(self, attr).data
        if not value is None:
            result = [0]*len(matches)
            for item in value.split(", "):
                for idx, match in enumerate(matches):
                    if re.search(match, item.lower()):
                        count = re.search(r"\d+", item)
                        result[idx] = 1 if not count else int(count.group())
            getattr(self, attr).data = result
        else:
            getattr(self, attr).data = None

    def type_prepare(self):
        value = getattr(self, "type")
        if not value.data is None:
            self.type.data = value.data.split(" ")[0]
        self.to_index("type")

    def material_prepare(self):
        self.to_index("material")

    def floors_prepare(self):
        value = getattr(self, "floors")
        if not value.data is None:
            value = value.data.split(" из ")
            if len(value) != 0:
                self.floors.data = value[1]
            self.to_int("floors")

    def owner_type_prepare(self):
        self.to_index("owner_type")

    def layout_prepare(self):
        self.to_index("layout")

    def condition_prepare(self):
        self.to_index("condition")

    def heating_type_prepare(self):
        self.to_index("heating_type")

    def year_prepare(self):
        value = getattr(self, "year").data
        if not value is None:
            value = value.split(" ")
            if len(value) > 1:
                self.year.data = value[-1]
            self.to_int("year")

    def owner_id_prepare(self):
        self.to_int("owner_id")

    def area_kitchen_prepare(self):
        self.to_float("area_kitchen")

    def area_living_prepare(self):
        self.to_float("area_living")

    def height_prepare(self):
        self.to_float("height")

    def elevators_prepare(self):
        self.to_list("elevators", ["нет", "пассажир", "груз"])

    def balconies_prepare(self):
        self.to_list("balconies", ["балкон", "лоджи"])

    def parkings_prepare(self):
        self.to_list("parkings", ["нет", "под", "наз|открыт", "мног"])

    def bathrooms_prepare(self):
        self.to_list("bathrooms", ["совмеще", r"изолиро|раздел"])

    def window_types_prepare(self):
        self.to_list("window_types", ["дво", "улиц"])

    def save(self, id_):
        attrs = ["owner_id",
                 "owner_type",
                 "area_kitchen",
                 "area_living",
                 "layout",
                 "height",
                 "bathrooms",
                 "window_types",
                 "balconies",
                 "condition",
                 "phones"]
        f = Flat.query\
                .filter_by(id=id_)\
                .first()
        for attr in attrs:
            if getattr(f, attr) is None:
                setattr(f, attr, getattr(self, attr).data)
        attrs = ["type",
                 "year",
                 "material",
                 "elevators",
                 "parkings",
                 "garbage_chute",
                 "heating_type",
                 "floors"]
        f.building.counters = f.building.counters or {}
        for attr in attrs:
            value = getattr(self, attr).data
            if not value is None:
                f.building.counters[attr] = f.building.counters.get(attr, {})
                if isinstance(value, list):
                    for idx, i in enumerate(value):
                        if i > 0:
                            f.building.counters[attr][str(idx)] = f.building.counters[attr].get(str(i).lower(), 0) + int(i > 0)
                else:
                    f.building.counters[attr][str(value).lower()] = f.building.counters[attr].get(str(value).lower(), 0) + 1
        flag_modified(f.building, "counters")
        db.session.add(f)
        db.session.commit()
        print("Updated building: {} and flat: {}".format(f.building.id, f.id), end="\r")


class FillBuildingColumn:
    """
    """

    def __init__(self, colum_name):
        self.__column_name = colum_name
        self.__query_data = dict()

    def load_buildings(self):
        query = Building.query\
                    .filter(getattr(Building, self.__column_name)==None,
                            Building.counters!=None)
        self.__buildings_df = pd.read_sql(query.statement, query.session.bind)
        return self

    def fill_strong_items(self):
        expression = self.__buildings_df.counters.apply(lambda c: c.get(self.__column_name)).isnull()
        for idx, value in self.__buildings_df.loc[~expression].iterrows():
            column = value.counters.get(self.__column_name)
            if len(column.keys()) == 1:
                column = list(column.keys())[0]
                if not column in self.__query_data.keys():
                    self.__query_data[column] = list()
                self.__query_data[column].append(value.id)
        return self

    def fill_medium_items(self):
        expression = self.__buildings_df.counters.apply(lambda c: c.get(self.__column_name)).isnull()
        for idx, value in self.__buildings_df.loc[~expression].iterrows():
            column = value.counters.get(self.__column_name)
            if len(column.keys()) > 1 and np.std(list(column.values())) > 0:
                mv = 0
                for k, v in column.items():
                    if v > mv:
                        mv = v
                        mk = k
                if not mk in self.__query_data.keys():
                    self.__query_data[mk] = list()
                self.__query_data[mk].append(value.id)
        return self

    def update_column(self):
        for key, ids in self.__query_data.items():
            query = """ UPDATE {}
                        SET {} = {}
                        WHERE id IN {}
                    """.format(Building.__table__, self.__column_name, key, tuple(ids) if len(ids) > 1 else "({})".format(ids[0]))
            db.engine.execute(query)


class FillBuildingCoords:
    """
    """

    YANDEX_GEO_URL = "https://geocode-maps.yandex.ru/1.x/"

    def __init__(self, building):
        self.__building = building

    def __get_city_name(self):
        for city, value in LABELS.get("cities").items():
            if int(value.get("id")) == self.__building.city:
                return city

    def fill_coords(self, city_name=None):
        r = requests.get(self.YANDEX_GEO_URL, params={
            "apikey": "b81f55cb-2d9c-44d5-8e0e-8be26be1f66e",
            "geocode": " ".join([self.__get_city_name(),
                                 self.__building.street,
                                 self.__building.house]),
            "format": "json",
            "results": 1
        })
        featureMember = r.json()\
                         .get("response", {})\
                         .get("GeoObjectCollection", {})\
                         .get("featureMember", [])
        if len(featureMember) > 0:
            lng, lat = featureMember[0].get("GeoObject", {})\
                                       .get("Point", {})\
                                       .get("pos", "").split(" ")
            self.__building.lng, self.__building.lat = float(lng), float(lat)
        return self

    def fill_district(self):
        if not self.__building.lng is None and not self.__building.lat is None:
            r = requests.get(self.YANDEX_GEO_URL, params={
                "apikey": "b81f55cb-2d9c-44d5-8e0e-8be26be1f66e",
                "geocode": ",".join([str(self.__building.lng),
                                     str(self.__building.lat)]),
                "format": "json",
                "type": "district"
            })
            district = r.json()\
                        .get("response")\
                        .get("GeoObjectCollection")\
                        .get("featureMember")
            for d in district:
                components = d.get("GeoObject")\
                              .get("metaDataProperty")\
                              .get("GeocoderMetaData")\
                              .get("Address")\
                              .get("Components")
                for c in components:
                    if c.get("kind") in ["district", "area"] and "административный округ" in c.get("name"):
                        district = c.get("name")
                        break
            districts = LABELS.get("cities").get(self.__get_city_name()).get("districts", [])
            if not isinstance(district, str):
                app.logger.error("{} Geocoder cant find district.".format(self.__building.id))
                return self
            if district in districts:
                self.__building.district = districts.index(district)
            else:
                app.logger.error("District: {} not found in allowed districts.".format(district))
        return self

    def update(self):
        db.session.add(self.__building)
        db.session.commit()
        print("Updated geography for: {}{}".format(", ".join([self.__building.street, self.__building.house]), " "*100), end="\r")
