import re


defaults = {
    "url_list": "https://www.cian.ru/cat.php",
    "url_item": "https://www.cian.ru/sale/flat/",
    "params": {
        # "district[0]": 1,
        # "district[0]": 4,
        # "district[0]": 5,
        # "district[0]": 6,
        # "district[0]": 7,
        # "district[0]": 8,
        # "district[0]": 9,
        # "district[0]": 10,
        # "district[0]": 11,
        # "district[0]": 151,
        # "district[0]": 325,
        # "district[0]": 326,
        "deal_type": "sale",
        "engine_version": 2,
        "offer_type": "flat",
        "region": 1,
        "p": 0
    },
    "target": {
        "link": {
            "href": re.compile(r"https://www.cian.ru/sale/flat/\d+/"),
            "class": re.compile("header")
        },
        "price": [
            {
                "class": re.compile("price_flex_container|price-flex-container")
            },
            {
                "class": re.compile("header")
            }
        ],
        "address": {
            "itemprop": re.compile("name")
        },
        "user": {
            "href": re.compile("id_user=")
        }
    },
    "owner_link": {
        "href": re.compile(r"(agents|company)\/\d+")
    },
    "owner_text": {
        "data-name": re.compile("HomeownerBlockAside")
    },
    "description": {
        "data-name": "Description"
    },
    "description_item": {
        "class": re.compile(r"info-title")
    },
    "phone": {
        "href": re.compile("tel:")
    },
    "features": {
        "data-name": re.compile("AdditionalFeatureItem")
    },
    "features-bti": {
        "data-name": re.compile("BtiHouseData")
    },
    "features-bti-item": {
        "class": re.compile("item")
    },
    "description_map": {
        "Жилая": "area_living",
        "Кухня": "area_kitchen",
        "Построен": "year"
    },
    "features_map": {
        "Тип жилья": "building.type",
        "Планировка": "room_type",
        "Высота потолков": "room_height",
        "Санузел": "bathroom_type",
        "Балкон/лоджия": "balcony",
        "Ремонт": "condition",
        "Вид из окон": "windows_type"
    },
    "features_bti_map": {
        "Лифты": "lifts",
        "Парковка": "parking_type",
        "Год постройки": "year",
        "Тип дома": "material",
        "Отопление": "heating_type",
        "Мусоропровод": "garbage_chute"
    }
}
