import json
from flask.cli import AppGroup

from application import app, db
from .models import ParserList, ParserItem
from ..dataset import labels as LABELS
from ..dataset.models import Flat


cian = AppGroup("cian")

@cian.command("create")
def cian_create():
    pl = ParserList()
    while not pl.is_blocked() and not pl.is_finished():
        pl.next_page().request_items()
    building_n, flat_n = 0, 0
    for item in pl.items:
        bn, fn = item.save()
        building_n += bn
        flat_n += fn
    app.logger.info("Passed {} pages, created buildings: {}, flats: {}.".format(pl.page, building_n, flat_n))


@cian.command("update")
def cian_update():
    pi = ParserItem()
    values = Flat.query.with_entities(Flat.external_id)\
                       .filter(Flat.source==LABELS.get("sources", {}).get("cian"),\
                               Flat.created_on==Flat.updated_on)\
                       .values("id", "external_id", "owner_id")
    for id_, external_id, owner_id in values:
        r = pi.request_item(external_id, owner_id)
        if r.is_blocked():
            break
        if hasattr(r, "form"):
            if r.form.validate():
                r.form.save(id_)
            else:
                app.logger.error(str(external_id) + " " + str(r.form.errors))

app.cli.add_command(cian)
