import re
import requests
import urllib.parse
from bs4 import BeautifulSoup
import json
from . import defaults
from random import randint

from application import app
from . import defaults as DEFAULTS
from ..dataset.forms import CreateForm, UpdateForm
from ..dataset import labels as LABELS


class ParserList:
    """ Класс создания экземпляров объявлений.
        Собирает обращается к ксписку объявлений на cian и
        с минимальными параметрами создает копии.
    """

    def __init__(self):
        self.__items = []
        self.__finished = False
        self.__blocked = False
        self.__params = DEFAULTS["params"]

    def __set_finished(self, url):
        """ При достижении конца списка объявлений cian перебрасывает сново на
            первую (переметр "p") страницу, это говорит что мы прошли до конца.
            :url адрес с параметрами который пернул cian
        """
        params = urllib.parse.urlparse(url)
        params = urllib.parse.parse_qsl(params.query)
        params = dict(params)
        self.__finished = not self.__params["p"] == int(params["p"])

    def __parse_link_to_parameters(self, element):
        """ Фнкция разбивает тег на параметры объявления.
            :element элемен содержимое которого берется за основу
        """
        rooms, area, floor, floors, room_type = None, None, None, None, None
        items = [ div.text for div in element.findAll("div") ]
        if len(items):
            items = items[-1].split(", ")
            rooms = re.findall(r"\d+", items[0])
            if len(rooms) > 0:
                rooms = int(rooms[0])
            else:
                rooms = 1
            area = items[1].split(" ")[0]
            area = float(area.replace(",", "."))
            floor = int(items[2].split("/")[0])
            floors = int(re.findall(r"\d+", items[2].split("/")[1])[0])
            studio = len(re.findall(r"(Студия)", items[0])) > 0
        return rooms, area, floor, floors, room_type

    def request_items(self):
        """ Получение списка объявлений и заполнение списка объявлений
            предварительной информацией. Данные берутся из содержимого страницы.
            Результатом работы метода является заполненный список __items.
        """
        try:
            r = requests.get(DEFAULTS["url_list"],
                             params=self.__params)
        except Exception as e:
            app.logger.error(e)
            self.__blocked = True
            return self
        dom = BeautifulSoup(r.content, "html.parser")
        if dom.title == None:
            app.logger.info("Tech works: " + r.url)
            self.__blocked = True
            return self
        if len(re.findall("Captcha", dom.title.text)):
            self.__blocked = True
            app.logger.info("Cian captcha detention: " + r.url)
        if len(re.findall("Технические работы", dom.title.text)):
            app.logger.info("Tech works: " + r.url)
            self.__blocked = True
        for link in dom.findAll("a", attrs=DEFAULTS["target"]["link"]):
            wrapper = link.parent.parent.parent.parent
            rooms, area, floor, floors, room_type  = self.__parse_link_to_parameters(link)
            price = wrapper.find("div", attrs=DEFAULTS["target"]["price"][0])\
                           .find("div", attrs=DEFAULTS["target"]["price"][1]).text
            price = re.findall(r"\d+", price)
            price = float("".join(price))
            external_id = link.get("href").replace(DEFAULTS["url_item"], "").replace("/", "")
            external_id = int(external_id)
            item = {
                "external_id": external_id,
                "source": "cian",
                "rooms": rooms,
                "area": area,
                "floor": floor,
                "price": price,
                "floors": floors
            }
            address = wrapper.find("span", attrs=DEFAULTS["target"]["address"])\
                             .get("content")\
                             .split(", ")
            if not address is None:
                item["city"] = address[0]
                item["street"] = address[-2]
                if "улица" in address[-1]:
                    item["street"] = address[-1]
                if len(address[-1]) < 10:
                    item["house"] = address[-1]
            owner = wrapper.find("a", attrs=DEFAULTS["target"]["user"])
            if not owner is None:
                owner = list(filter(lambda s: "id_user" in s, owner.get("href").split("&")))
                owner = owner[0].split("=")[1]
                item["owner_id"] = int(owner)
            form = CreateForm(data=item)
            if form.validate():
                self.__items.append(form)
            else:
                app.logger.error(str(external_id or "") + " " + json.dumps(form.errors))
        self.__set_finished(r.url)
        return self

    def is_finished(self):
        return self.__finished

    def is_blocked(self):
        return self.__blocked

    @property
    def page(self):
        return self.__params.get("p", 0)
    

    def next_page(self):
        """ Метод для переключения страницы, следом выполняется request_items
            для загруки данных.
        """
        self.__params["p"] += 1
        print("Current page: {}".format(self.__params["p"]), end="\r")
        return self

    @property
    def items(self):
        return self.__items


class ParserItem:
    """
    """

    def __init__(self):
        self.__blocked = False

    def get_feature(self, dom, anchors):
        description = dom.find("div", attrs={"data-name": "Description"})
        if not description is None:
            description = description.find("div", attrs={"class": re.compile("info-block")})
            if not description is None:
                for item in description.findAll("div", attrs={"class": re.compile("info--")}):
                    if item.findAll("div")[-1].text in anchors:
                        return item.find("div").text
        bti_house_data = dom.find("div", attrs={"data-name": "BtiHouseData"})
        if not bti_house_data is None:
            for item in bti_house_data.findAll("div", attrs={"class": re.compile("item")}):
                if item.div.find(text=True, recursive=False) in anchors:
                    return item.findAll("div")[-1].text
        additional_features_group = dom.find("article", attrs={"data-name": "AdditionalFeaturesGroup"})
        if not additional_features_group is None:
            for item in additional_features_group.findAll("li"):
                if item.span.find(text=True, recursive=False) in anchors:
                    return item.findAll("span")[-1].text

    def get_owner(self, dom, owner_id=None, owner_type=None):
        container = dom.find("div", attrs={"data-name": re.compile(r"AuthorAsideBrand|HomeownerBlockAside")})
        if not container is None:
            if owner_id is None:
                link = container.find("a", attrs={"href": re.compile("company|agents")})
                name = container.find("h2")
                if not link is None:
                    paths = [path for path in link.get("href").split("/") if path != ""]
                    owner_id = paths[-1]
                    owner_type = paths[-2]
                if not name is None and re.compile(r"ID\ \d+").match(name.text):
                    owner_id = re.search(r"\d+", container.find("h2").text).group()
                    owner_type = "owner"
            if owner_type is None:
                button = container.find("div", attrs={"class": re.compile(r"honest-container")})
                if not button is None and not button.span is None and button.span.text == "Застройщик":
                    owner_type = "developer"
        return owner_id, owner_type

    def get_phones(self, dom):
        phones = dom.findAll("a", attrs=DEFAULTS["phone"])
        phones = map(lambda element: int(re.search(r"\d+", element.get("href")).group()),
                     phones)
        return list(phones)

    def request_item(self, external_id, owner_id):
        """
        """
        try:
            r = requests.get(DEFAULTS["url_item"] + str(external_id))
        except Exception as e:
            app.logger.error(e)
            return self
        dom = BeautifulSoup(r.content, "html.parser")
        if dom.title is None:
            self.__blocked = True
            app.logger.info("Cian captcha detention: " + r.url)
            return self
        if len(re.findall("Captcha", dom.title.text)):
            self.__blocked = True
            app.logger.info("Cian captcha detention: " + r.url)
        if len(re.findall("Технические работы", dom.title.text)):
            self.__blocked = True
            app.logger.info("Tech works: " + r.url)
            return self
        garbage_chute = self.get_feature(dom, ["Мусоропровод"])
        owner_id, owner_type = self.get_owner(dom, owner_id)
        self.form = UpdateForm(data=dict(type=self.get_feature(dom, ["Тип жилья"]),
                                         year=self.get_feature(dom, ["Год постройки", "Построен", "Срок сдачи"]),
                                         material= self.get_feature(dom, ["Тип дома"]),
                                         floors= self.get_feature(dom, ["Этаж"]),
                                         elevators=self.get_feature(dom, ["Лифты"]),
                                         parkings=self.get_feature(dom, ["Парковка"]),
                                         garbage_chute=None if garbage_chute is None else garbage_chute == "Есть",
                                         heating_type=self.get_feature(dom, ["Отопление"]),
                                         owner_id=owner_id,
                                         owner_type=owner_type,
                                         area_kitchen=self.get_feature(dom, ["Кухня"]),
                                         area_living=self.get_feature(dom, ["Жилая", "Площадь комнат"]),
                                         layout=self.get_feature(dom, ["Планировка"]),
                                         height=self.get_feature(dom, ["Высота потолков"]),
                                         bathrooms=self.get_feature(dom, ["Санузел"]),
                                         window_types=self.get_feature(dom, ["Вид из окон"]),
                                         balconies=self.get_feature(dom, ["Балкон/лоджия"]),
                                         condition=self.get_feature(dom, ["Ремонт"]),
                                         phones=self.get_phones(dom)))
        self.form.type_prepare()
        self.form.year_prepare()
        self.form.material_prepare()
        self.form.floors_prepare()
        self.form.elevators_prepare()
        self.form.parkings_prepare()
        self.form.heating_type_prepare()
        self.form.owner_id_prepare()
        self.form.owner_type_prepare()
        self.form.area_kitchen_prepare()
        self.form.area_living_prepare()
        self.form.layout_prepare()
        self.form.height_prepare()
        self.form.bathrooms_prepare()
        self.form.window_types_prepare()
        self.form.balconies_prepare()
        self.form.condition_prepare()
        return self

    def is_blocked(self):
        return self.__blocked
