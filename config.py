import os
import logging
import dotenv


dotenv.load_dotenv('.flaskenv')

logging.getLogger("werkzeug").disabled = True

logging.basicConfig(
    filename="logs/app.log",
    format="%(asctime)s - %(levelname)s %(message)s",
    level=logging.INFO
)

class Config:

    SQLALCHEMY_DATABASE_URI = "postgresql://{}:{}@{}:{}/{}".format(*[
        os.getenv("POSTGRES_USER"),
        os.getenv("POSTGRES_PASSWORD"),
        os.getenv("POSTGRES_HOST"),
        os.getenv("POSTGRES_PORT"),
        os.getenv("POSTGRES_NAME"),
    ])

    SQLALCHEMY_TRACK_MODIFICATIONS = False

